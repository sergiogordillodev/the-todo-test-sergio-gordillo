Tengo que:

· Investigar sobre distintos diseños de to-dos para detallar las funcionalidades de la app y el diseño
· En base a eso, definir un poco que necesito sobre la arquitectura que ya tengo implementada
· Tras eso, tener una lista y ponerme a implementar

· App.tsx = aquí voy a manejar mi estado y a llamar a mis hijos

COMPONENTES=

· ToDoAdd = componente para añadir
· ItemsList= componente de la lista
· ItemsListRow = componente de los ítems de la lista

· Hooks = ToDoReducer, useForm;  

HOW TO WORK=

· Primero me creo el ToDoAdd en la app principal y luego me lo llevo a componente. Quiero primero que visualmente quede bien.

//TODO: Me falta añadir el icono de delete en la lista
//TODO: Centrarme en add, delete (funcionalidad aunque me falte el dispatch) y llamada a API
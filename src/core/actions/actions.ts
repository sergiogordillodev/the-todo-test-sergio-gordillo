import { ToDoAPIModel } from "@/api/to-do.model";
import {actionIds} from "./actions-id";

export const addToDo = (todo: any) => ({
    type: actionIds.ADD_TO_DO, 
    action: todo
});

export const loadToDoCollection = (toDosCollection: ToDoAPIModel[]) => ({
    type: actionIds.LOAD_TO_DO_COLLECTION,
    action: toDosCollection
})
    


    

import { combineReducers } from "redux";
import { toDoReducer, ToDoState } from './to-do-reducer';

export interface State {
  toDoReducer: ToDoState
}

export const reducers = combineReducers<State>({
  toDoReducer
});

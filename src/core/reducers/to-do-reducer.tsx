import { actionIds } from "../actions";
import { ActionBase } from "../actions";

export type ToDoState = any[];

export const toDoReducer = (state: ToDoState = [], action: ActionBase): any => {
  switch (action.type) {
    case actionIds.ADD_TO_DO:
      return [...state, action.payload];
    case actionIds.DELETE_TO_DO:
      return state.filter((todo) => todo.id !== action.payload); //TODO: Repasar esta lógica con el vídeo de Fernando Herrera
    case actionIds.TOGGLE_TO_DO:
      return state.map((todo) =>
        todo.id === action.payload ? { ...todo, done: !todo.done } : todo //TODO: Repasar esta lógica con el vídeo de Fernando Herrera porque no acabo de comprender lo de comparar una propiedad (todo.id) con el objeto completo (action.payload)
      );
    default:
      return state;
  }
};

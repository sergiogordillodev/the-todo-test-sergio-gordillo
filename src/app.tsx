import TextField from "@mui/material/TextField/TextField";
import React from "react";
import { ItemsList } from "@/pods/todo";

export const App = () => {
  return (
    <>
      <ItemsList />
    </>
  );
};

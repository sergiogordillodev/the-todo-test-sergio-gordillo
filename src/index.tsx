import React from "react";
import { createRoot } from "react-dom/client";

import { createStore } from "redux";
import { Provider } from "react-redux";
import { reducers } from "./core/reducers";

import { App } from "./app";

const container = document.getElementById("root");
const root = createRoot(container);

const store = createStore(
  reducers,
  window["__REDUX_DEVTOOLS_EXTENSION__"] &&
    window["__REDUX_DEVTOOLS_EXTENSION__"]()
);

root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

import React from "react";

interface Props {
  children: React.ReactNode;
}

export const List: React.FC<Props> = ({ children }) => {
  return (
    <>
      <div className="container">
        <div className="title">
          <h2>Which things do you have to do today?</h2>
        </div>
        {children}
      </div>
    </>
  );
};

//TODO: Este componente no se está mostrando ahora mismo
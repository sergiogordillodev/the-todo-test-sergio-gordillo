import { ToDoAPIModel } from "./to-do.model";
import { ToDoVM } from "@/pods/todo/todo.vm";
import { useDispatch } from "react-redux";
import { actionIds } from "@/core/actions";

const dispatch = useDispatch();

export const mapToDoFromAPIModelToVM = (todo: ToDoAPIModel): ToDoVM => {
    let newToDo = {...todo}; 
    delete newToDo.userid;
    return newToDo; 
};

export const mapToDoFromAPIModelToVMDispatch = (todo: ToDoAPIModel): void => {
    let newToDo = {...todo}; 
    delete newToDo.userid;
    dispatch({
        type: actionIds.ADD_TO_DO, 
        payload: newToDo
    })
};



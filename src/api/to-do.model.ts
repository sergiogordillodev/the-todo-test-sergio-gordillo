export interface ToDoAPIModel {
    userid: number,
    id: number;
    title: string;
    completed: boolean;
}
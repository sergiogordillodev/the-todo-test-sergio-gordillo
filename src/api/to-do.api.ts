import { ToDoAPIModel } from "./to-do.model";
import { useDispatch } from 'react-redux';
import { actionIds } from "@/core/actions";
import { mapToDoFromAPIModelToVM } from './to-do.mapper';

const dispatch = useDispatch();

export const getToDos = (): Promise<ToDoAPIModel[]> => {
    return fetch(`https://jsonplaceholder.typicode.com/todos`)
        .then((response) => {
            if (response.status == 200) {
                const data = response.json();
                return data;
            } else {
                throw new Error(response.statusText)
            }
        })
        .catch((e) => console.log(e));
}

// export const getToDosDispatch = (): Promise<void> => {
//     return fetch(`https://jsonplaceholder.typicode.com/todos/1`)
//         .then((response) => {
//             if (response.status == 200) {
//                 const data = response.json();
//                 const dataVM = mapToDoFromAPIModelToVM(data);
//                 dispatch({
//                     type: actionIds.ADD_TO_DO, 
//                     payload: dataVM
//                 })
//             } else {
//                 throw new Error(response.statusText)
//             }
//         })
//         .catch((e) => console.log(e));
// }
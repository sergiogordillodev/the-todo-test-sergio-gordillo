import React from "react";

import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table/Table";
import TableBody from "@mui/material/TableBody/TableBody";
import TableCell from "@mui/material/TableCell/TableCell";
import TableContainer from "@mui/material/TableContainer/TableContainer";
import TableHead from "@mui/material/TableHead/TableHead";
import TableRow from "@mui/material/TableRow/TableRow";
import { useSelector, useDispatch } from "react-redux";

import { ToDoAdd, ItemListRow } from "./components";
import { State } from "@/core/reducers";
import { ToDoState } from "@/core/reducers/to-do-reducer";
import { getToDos } from "../../api/to-do.api";
import { loadToDoCollection } from "@/core/actions";

export const ItemsList: React.FC = () => {
  const dispatch = useDispatch();

  const todoList: any = useSelector<State, ToDoState>(
    (state) => state.toDoReducer
  );

  const loadToDos = async () => {
    const ToDosCollection = await getToDos();
    dispatch(loadToDoCollection(ToDosCollection));
  };

  React.useEffect(() => {}, []);

  return (
    <>
      <ToDoAdd />

      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 300 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="left">Id</TableCell>
              <TableCell align="left">Title</TableCell>
              <TableCell align="left">Completed</TableCell>
              <TableCell align="left"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {todoList.map((item) => (
              <ItemListRow key={item.id} item={item} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

//import { getToDos } from "../api";
//import { mapToDoFromAPIModelToVM } from "../api/to-do.mapper";

/*
  React.useEffect(() => {
    getToDos().then((data) => {
      const toDoVM = mapToDoFromAPIModelToVM(data);
      //aquí lanzaríamos otro dispatch, pero no sé de dónde sacarlo
    });
  }, []);
*/

import React from "react";
import { useDispatch } from "react-redux";
import Box from "@mui/material/Box/Box";
import Button from "@mui/material/Button/Button";
import TextField from "@mui/material/TextField/TextField";
import { actionIds } from "@/core/actions";

import { ToDoVM } from "../todo.vm";

export const ToDoAdd: React.FC = () => {
  const [text, setText] = React.useState("");
  const dispatch = useDispatch();

  const handleClick = (text: string) => {
    if (text.trim().length <= 1) {
      return;
    }

    const newToDo: ToDoVM = {
      id: new Date().getTime(),
      title: text,
      completed: false,
    };

    dispatch({ type: actionIds.ADD_TO_DO, payload: newToDo });
  };

  return (
    <>
      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "25ch" },
          ml: { xs: "none", md: 6, lg: 8, xl: 11 },
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          id="outlined-basic"
          label="Write your To Do here"
          variant="outlined"
          value={text}
          onChange={(e) => setText(e.target.value)}
        ></TextField>
        <Button
          variant="contained"
          sx={{
            minHeight: 56,
          }}
          onClick={() => handleClick(text)}
        >
          Add
        </Button>
      </Box>
    </>
  );
};

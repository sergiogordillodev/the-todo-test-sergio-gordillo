import React from "react";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import DeleteIcon from "@mui/icons-material/Delete";

import { useDispatch, useSelector } from "react-redux";
import { State } from "@/core/reducers";
import { ToDoState } from "@/core/reducers/to-do-reducer";
import { getToDos } from "../../../api/to-do.api";
import {
  mapToDoFromAPIModelToVM,
  mapToDoFromAPIModelToVMDispatch,
} from "../../../api/to-do.mapper";
import { ToDoVM } from "../todo.vm";

interface Props {
  item: ToDoVM;
}

export const ItemListRow: React.FC<Props>= (props) => {

  const { item } = props;

  return (
    <>
        <TableRow>
          <TableCell>{item.id}</TableCell>
          <TableCell>
            <span>{item.title}</span>
          </TableCell>
          <TableCell>
            <span>{item.completed.toString()}</span>
          </TableCell>
          <TableCell>
            <DeleteIcon />
          </TableCell>
        </TableRow>
    </>
  );
};

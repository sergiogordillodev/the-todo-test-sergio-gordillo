export interface ToDoVM {
  id: number;
  title: string;
  completed: boolean;
}